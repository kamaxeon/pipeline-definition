#!/bin/bash
set -euo pipefail

function say { echo "$@" | toilet -f mono12 -w 300 | lolcat -f || echo "$@"; }
function echo_green { echo -e "\e[1;32m${1}\e[0m"; }
function echo_red { echo -e "\e[1;31m${1}\e[0m"; }
function echo_yellow { echo -e "\e[1;33m${1}\e[0m"; }

say "bootstrap"

PACKAGES=(pyyaml yamllint git+https://gitlab.com/cki-project/cki-lib.git/)

if [[ "$(type -P python3)" = /usr* ]]; then
    python3 -m pip install --user "${PACKAGES[@]}"
else
    python3 -m pip install "${PACKAGES[@]}"
fi

declare -i FAILED=0

predefined=(CI_JOB_ID CI_JOB_STAGE architectures brew_task_id cover_letter make_target
    merge_branch nvr patch_urls server_url status target_branch target_repo top_url
    tree_name web_url rpm_release)

echo_yellow "Checking cki_pipeline.yml"
declare -a predefined_args
for i in "${predefined[@]}"; do
    predefined_args+=(--predefined "$i")
done
if ! gitlab-yaml-shellcheck --check-sourced "${predefined_args[@]}" cki_pipeline.yml; then
    FAILED+=1
fi

for i in tests/*.sh; do
    echo_yellow "Checking $i"
    if ! shellcheck -axf gcc "$i"; then
        FAILED+=1
    fi
done

for i in .gitlab-ci.yml cki_pipeline.yml trees/*.yml; do
    echo_yellow "Checking $i"
    if ! yamllint -s "$i"; then
        FAILED+=1
    fi
done

if [ "${FAILED}" -gt 0 ]; then
    echo_red "$FAILED linting steps failed."
    exit 1
fi
